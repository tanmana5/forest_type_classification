# forest_type_classification

Repo to implement and learn regression and classification in TF. 
Application to problem of multi-class classification:

**Objective**: Classify different forest cover types (4-class classification).
			   Dataset description at https://archive.ics.uci.edu/ml/datasets/covertype
			   

**Notebook can be viewed at:
http://nbviewer.jupyter.org/github/tanmana5/forest-type-classification/blob/master/forest_cover_classification.ipynb**

